FROM ubuntu:16.04 RUN apt-get update && apt-get -y upgrade RUN apt-get -y install python3 python3-venv python3-dev python3-pip git

RUN mkdir /home/devops WORKDIR /home/devops RUN git clone https://github.com/neva-nevan/website.git

COPY boot.sh website/boot.sh RUN chmod +x website/boot.sh

WORKDIR /home/devops/website RUN pip3 install -r requirments.txt RUN pip3 install gunicorn

ENV FLASK_APP=microblog.py

EXPOSE 5000 ENTRYPOINT ["/bin/bash", "/home/devops/website/boot.sh"]

### boot.sh

#!/bin/sh cd /home/devops/website gunicorn -b :5000 --access-logfile - --error-logfile - microblog:app