from flask import render_template, flash, redirect, url_for, request, jsonify
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse
import requests

from pathlib import Path
from jinja2 import Environment, FileSystemLoader
import yaml, json

from datetime import datetime
from app.get_ext_repo import get_ext_repo

from app import app, db
from app.models import User, Post
from app.forms import *

#--- Run before other view functions


@app.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()


#--- Register New User


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('simple_form.html', title='Register', form=form)


#--- User Session Management


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('simple_form.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))











@app.route('/.well-known/acme-challenge/<token>', methods=['GET'])
def le(token):
    return str(token)










#--- General Website


@app.route('/render', methods=['GET', 'POST'])
def render():

    repo_dir = 'app/repo/'
    debug = ''

    if request.get_json():
        received = request.get_json()
        repo_url = yaml.load(received["repo_url"])

        ext_repo_info = get_ext_repo(repo_url)

        # Convert python dict to JSON, so AJAX can read it.
        result = jsonify(ext_repo_info)
        return result

    foundtemplates = fnmatch.filter(os.listdir(repo_dir), '*.j2')
    return render_template('renderform.html',  title='Render Template', foundtemplates=foundtemplates, debug=debug)


@app.route('/process', methods=['POST'])
def process():

    received = request.get_json()
    r = requests.get(received["template"])

    if '---' not in received["answers"]:
        return 'Answers must begin with ---'

    with open("app/repo/render.tmp", "w") as file:
        file.write(r.text)

    # Load data from YAML into Python dictionary
    answerfile = yaml.load(received["answers"])

    # Load Jinja2 template
    env = Environment(loader=FileSystemLoader('app/repo/'), trim_blocks=True, lstrip_blocks=True)
    template = env.get_template("render.tmp")

    # Render the template

    try:
        rendered_template = template.render(answerfile)

    except Exception as e:
        return str(e)

    #print(rendered_template)

    return str(rendered_template)


@app.route('/show', methods=['POST'])
def show():

    received = request.get_json()
    answerfile = str(received[0]['value']).replace('.j2', '.yml')
    r = requests.get(answerfile)

    return r.text


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
#@login_required
def index():
    form = PostForm()
    group = None

    if form.validate_on_submit():
        post = Post(body=form.post.data, title=form.title.data, categories=form.categories.data, code = form.code.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        flash('Your post is now live!')
        return redirect(url_for('index'))

    #--- Get followed users posts
    posts = Post.query.order_by(Post.timestamp.desc()).all()
    if current_user.is_authenticated:
        group = current_user.group
    return render_template('index.html', title='Home', posts=posts, form=form, group=group)


@app.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    posts = Post.query.order_by(Post.timestamp.desc()).all()
    return render_template('user.html', user=user, title = 'About', posts=posts)


@app.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('user', username=current_user.username))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('simple_form.html', title='Edit Profile', form=form)


@app.route('/edit_pw', methods=['GET', 'POST'])
@login_required
def edit_pw():
    form = EditPw()
    if form.validate_on_submit():
        user = current_user
        if user is None or not user.check_password(form.current_password.data):
            flash('Current password was incorrect.')
            return redirect(url_for('edit_pw'))
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Your password has been updated.')
        return redirect(url_for('logout'))
    return render_template('simple_form.html', title='Change your password', form=form)


@app.route('/edit_post/<post>', methods=['GET', 'POST'])
@login_required
def edit_post(post):
    form = PostForm()
    delete = DeletePost()
    group = None
    edit_check = False
    found_post = Post.query.get(post)

    if delete.validate_on_submit() and delete.post_id.data != None:
        selected_post = Post.query.get(delete.post_id.data)
        db.session.delete(selected_post)
        db.session.commit()
        flash('Deleted {0}'.format(delete.post_id.data))
        return redirect(url_for('index'))

    elif form.validate_on_submit() and delete.post_id.data == None:
        found_post.body = form.post.data
        found_post.title = form.title.data
        found_post.categories = form.categories.data
        found_post.code = form.code.data
        #post = Post(body=form.post.data, title=form.title.data, code = form.code.data, author=current_user)
        #db.session.add(post)
        db.session.commit()
        flash('The post has been updated.')
        return redirect(url_for('index'))

    elif request.method == 'GET':
        form.post.data = found_post.body
        form.title.data = found_post.title
        form.categories.data = found_post.categories
        form.code.data = found_post.code
        edit_check = True
        post = Post.query.get(post)

    if current_user.is_authenticated:
        group = current_user.group
    return render_template('index.html', title='Home', post=post, form=form, delete=delete, group=group, edit_check=edit_check)


@app.route('/follow/<username>')
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User {} not found.'.format(username))
        return redirect(url_for('index'))
    if user == current_user:
        flash('You cannot follow yourself!')
        return redirect(url_for('user', username=username))
    current_user.follow(user)
    db.session.commit()
    flash('You are following {}!'.format(username))
    return redirect(url_for('user', username=username))


@app.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User {} not found.'.format(username))
        return redirect(url_for('index'))
    if user == current_user:
        flash('You cannot unfollow yourself!')
        return redirect(url_for('user', username=username))
    current_user.unfollow(user)
    db.session.commit()
    flash('You are not following {}.'.format(username))
    return redirect(url_for('user', username=username))