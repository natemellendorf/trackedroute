from flask_wtf import FlaskForm
from wtforms import IntegerField
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextAreaField, SelectField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length
from app.models import User
from flask_ckeditor import CKEditorField
from flask_wtf.file import FileField, FileRequired, FileAllowed
import os, fnmatch

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class EditProfileForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    about_me = TextAreaField('About me', validators=[Length(min=0, max=140)])
    submit = SubmitField('Submit')

    def __init__(self, original_username, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username

    def validate_username(self, username):
        if username.data != self.original_username:
            user = User.query.filter_by(username=self.username.data).first()
            if user is not None:
                raise ValidationError('Please use a different username.')


class EditPw(FlaskForm):
    current_password = PasswordField('Current password', validators=[DataRequired()])
    password = PasswordField('New password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Update')


class PostForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired(), Length(min=1, max=64)])
    categories = StringField('Categories', validators=[DataRequired(), Length(min=1, max=128)])
    post = CKEditorField('Body', validators=[DataRequired(), Length(min=1, max=2000)])
    code = StringField('Code', validators=[Length(min=0, max=200)])
    submit = SubmitField('Submit')

class DeletePost(FlaskForm):
    post_id = IntegerField('Post Number:')
    submit = SubmitField('Delete?')

class EditPostForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    about_me = TextAreaField('About me', validators=[Length(min=0, max=140)])
    submit = SubmitField('Submit')

    def __init__(self, original_username, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username




class DynamicForm(FlaskForm):
    templatelist=[]
    clientlist=[]

    foundtemplates = fnmatch.filter(os.listdir('app/repo/'), '*.j2')
    foundclients = ['', 1, 2, 3]

    templatelist.append(('---', '---'))

    for value in foundclients:
        clientlist.append((value, value))

    for value in foundtemplates:
        templatelist.append((value, value))

    #clientnumber = SelectField('Client #', choices=clientlist)
    templatefile = SelectField('Select Template', id='template', choices=templatelist, validators=[DataRequired()])
    dynamic_af = TextAreaField('Input Answers', id='dynamic_af', render_kw={'class': 'form-control', 'rows': 10}, validators=[DataRequired()])
    #answerfile = FileField('Upload Answers', validators=[FileRequired()])
    #submit = SubmitField('Submit')
