import requests
from pprint import pprint

def get_ext_repo(ext_url):

    error_results = dict()

    if 'github.com' not in ext_url:
        error_results['error'] = 'URL must contain github.com'
        return error_results

    try:
        # Convert user provided url to API url.
        ext_url = ext_url.replace('https://github.com', 'https://api.github.com/repos')

        # With requests, get basic info on repo.
        r = requests.get(ext_url)

        if 'API rate limit exceeded' in r.text:
            error_results['error'] = 'API rate limit exceeded'
            return error_results

        ext_repo_info = r.json()

        # With Requests, get a list of all files in the repo.
        r = requests.get(ext_url + '/contents/')

        d = r.json()
        # Loop over the dictionary we acquired with d, and put interesting info in repo dict.
        ext_repo_files = {}
        for path in d:
            if '.j2' in path['path']:
                ext_repo_files[path['path']] = path['download_url']

        ext_repo_info['files'] = ext_repo_files

        return ext_repo_info

    except Exception as e:
        error_results['error'] = 'Unable to access GitHub repo...'
        return error_results

if __name__ == '__main__':
    test = get_ext_repo('https://github.com/natemellendorf/tr_templates')
    pprint(test)
